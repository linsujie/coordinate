#ifndef CONSTANT_H
#define CONSTANT_H

const double LHAASO_Lati = 29.357656306,
      LHAASO_Lngi = 100.138794639,
      LHAASO_Alti = 4372.07+4.3,
      LHAASO_phi = -0.558,
      WCDA_phi = 29.442,
      YBJ_Lati = 30.102,
      YBJ_Lngi = 90.522,
      Crab_Lati = 22.0145,
      Crab_Ra = (5 + (34.5 / 60)) * 180 / 12, // 5h34.5m -> degree
      Mrk421_Lati = 38.208833,
      Mrk421_Ra = (11 + (4.0 / 60 + 27.314 / 3600)) * 180 / 12, // 11h4m27.314s -> degree
      pi = 3.14159265358979323846,
      deg2rad = pi / 180,
      rad2deg = 180 / pi;

#endif /* CONSTANT_H */

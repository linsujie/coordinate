#include<cmath>

#include "conversions.h"

#ifndef NO_SOFA
#include "sofa.h"
#endif

using namespace std;

/********** GMST(mjd) **********/
double gmst(double mjd) { // unit: day
  double ut1, Tu, alpham;

  ut1 = mjd - floor(mjd);
  Tu = ( mjd - 51544.5 ) / 36525.0;
  alpham = 280.460618370 
    + 36000.770053608 * Tu 
    + 3.8793333331e-4 * Tu * Tu 
    - 2.5833333331e-8 * Tu * Tu * Tu;

  double result = ( 180.0 + ut1 * 360.0  + alpham );
  return result / 360 - int(result / 360);
}

/********** LMST(mjd) **********/
double lmst(double mjd) {
  double result = gmst(mjd) + LHAASO_Lngi / 360.0;
  return result - int(result);
}

/********* local earth rotation angle *********/
double lera(double mjd) {
#ifdef NO_SOFA
  double Tu = mjd - 51544.5;
  double rspeed = 1.00273781191135448;
  double ra2000 = 0.7790572732640;
  double result = ra2000 + rspeed * Tu + LHAASO_Lngi / 360.0;
#else
  double result = iauEra00(2400000.5, mjd) / 2 / pi + LHAASO_Lngi / 360.0;
#endif

  return (result - int(result)) * 2 * pi;
}

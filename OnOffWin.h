#ifndef ONOFFWIN_H
#define ONOFFWIN_H
#include <map>
#include <vector>
#include <functional>
#include "coordinate.h"

/**********************************************************************
 * This class is used to build the on-off windows for specified source
 * and tell the user if an event falls inside these window.
 * The off window number noff must be even, the window order ranges from 
 * -noff/2 to noff/2. The zero is the on window.
 *********************************************************************/
struct h_point { double zenc, azic; };
class OnOffWin {
  public:
    enum arrange_type { concentrate, average };

    inline int id2i(int id) const { return id + half_noff; }
    inline int i2id(int i) const { return i - half_noff; }

    // ra, dec, size in unit [deg]
    OnOffWin(double ra_, double dec_, double size_, int noff_, const coordinate& coord_);
    OnOffWin(void (*source_orbit_)(double, double&, double&), double size_, int noff_, const coordinate& coord_);

    // zenc, azic, in unit [deg]
    bool getid(double zenc, double azic, double mjd, int& win_id);
    bool count_dAngles(double zenc, double azic, double mjd, std::vector<int>& win_ids, std::vector<double>& dAngles);
    bool count_dAngles2d(double zenc, double azic, double mjd, std::vector<int>& win_ids, std::vector<double>& dAnglesx, std::vector<double>& dAnglesy);
    bool count_axis(double zenc, double azic, double mjd, std::vector<int>& win_ids, std::vector<double>& ras, std::vector<double>& decs);
    std::map<double, std::vector<h_point> >::iterator get_point(double mjd);
    void set_arrange_type(const arrange_type& t);

  private:
    int half_noff;
    double ra, dec, size;
    void (*source_orbit)(double, double&, double&);
    arrange_type rtype;
    coordinate coord;
    std::map<double, std::vector<h_point> > traces;

    inline double dazi(double wsize, double zen, const arrange_type rtype) const {
      double res = rtype == concentrate ? 2 * asin(sin(wsize) / sin(zen)) : 2 * pi / (half_noff * 2 + 1);
      assert(res == res && "The window size should be smaller enough to find the Off window.");

      return res;
    }
};

#endif /* ONOFFWIN_H */

#ifndef CONVERSIONS_H
#define CONVERSIONS_H

#include "constant.h"

double gmst(double mjd);
double lmst(double mjd);
double lera(double mjd);

#endif /* CONVERSIONS_H */

#ifndef SUN_ORBIT_H
#define SUN_ORBIT_H

//                             rad           rad
void sun_orbit(double mjd, double& SRAS, double& SDEC);

#endif /* SUN_ORBIT_H */

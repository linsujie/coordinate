#include <iostream>
#include <assert.h>
#include "OnOffWin.h"

using namespace std;
const double dmjd = 1.0 / 18000;

void default_orbit(double mjd, double &ra, double &dec) {};
OnOffWin::OnOffWin(double ra_, double dec_, double size_, int noff_, const coordinate& coord_)
  : ra(ra_ * deg2rad), dec(dec_ * deg2rad), size(size_ * deg2rad),
  source_orbit(&default_orbit),
  half_noff(noff_/2), coord(coord_), rtype(concentrate)
{}

OnOffWin::OnOffWin(void (*source_orbit_)(double, double&, double&), double size_, int noff_, const coordinate& coord_)
  : size(size_ * deg2rad), source_orbit(source_orbit_), half_noff(noff_/2), coord(coord_), rtype(concentrate)
{}

map<double, vector<h_point> >::iterator OnOffWin::get_point(double mjd) {
  auto result = traces.find(mjd);
  if (result == traces.end()) {
    source_orbit(mjd, ra, dec);
    coord.set_equator(ra, dec, mjd);

    vector<h_point> points(half_noff * 2 + 1);
    for (int i = 0; i < points.size() ; i++) {
      int id = i2id(i);
      points[i] = { coord.zen(), coord.azimuth() + id * dazi(size, coord.zen(), rtype) };
    }

    traces[mjd] = points;
    result = traces.find(mjd);
  }

  return result;
}

bool OnOffWin::getid(double zenc, double azic, double mjd, int& win_id) {
  zenc *= deg2rad;
  azic *= deg2rad;

  int nlow = (mjd - int(mjd)) / dmjd;
  double mjd_low = int(mjd) + nlow * dmjd,
         mjd_up = int(mjd) + (nlow + 1) * dmjd;

  auto plow = get_point(mjd_low),
       pup = get_point(mjd_up);

  double low_weight = (mjd_up - mjd) / (mjd_up - mjd_low),
         up_weight = 1 - low_weight;
  for (int i = 0; i < plow->second.size(); i++) {
    double zenc_center = low_weight * plow->second[i].zenc + up_weight * pup->second[i].zenc,
           azic_center = low_weight * plow->second[i].azic + up_weight * pup->second[i].azic;
    if (angle_distance(pi / 2 - zenc, pi / 2 - zenc_center, azic - azic_center) < size) {
      win_id = i2id(i);
      return true;
    }
  }

  return false;
}

bool OnOffWin::count_dAngles(double zenc, double azic, double mjd, vector<int>& win_ids, vector<double>& dAngles) {
  zenc *= deg2rad;
  azic *= deg2rad;

  int nlow = (mjd - int(mjd)) / dmjd;
  double mjd_low = int(mjd) + nlow * dmjd,
         mjd_up = int(mjd) + (nlow + 1) * dmjd;

  auto plow = get_point(mjd_low),
       pup = get_point(mjd_up);

  double low_weight = (mjd_up - mjd) / (mjd_up - mjd_low),
         up_weight = 1 - low_weight;
  win_ids.resize(0);
  dAngles.resize(0);
  for (int i = 0; i < plow->second.size(); i++) {
    double zenc_center = low_weight * plow->second[i].zenc + up_weight * pup->second[i].zenc,
           azic_center = low_weight * plow->second[i].azic + up_weight * pup->second[i].azic;
    double dangle = angle_distance(pi / 2 - zenc, pi / 2 - zenc_center, azic - azic_center);
    if (dangle > size) continue;

    win_ids.push_back(i2id(i));
    dAngles.push_back(dangle * rad2deg);
  }

  return !win_ids.empty();
}

struct vec {
  double x, y, z;
  const vec& operator-=(const vec& rhs) { x -= rhs.x; y -= rhs.y; z -= rhs.z; return *this; }
  const vec& operator+=(const vec& rhs) { x += rhs.x; y += rhs.y; z += rhs.z; return *this; }
  const vec& operator*=(double rhs) { x *= rhs; y *= rhs; z *= rhs; return *this; }
  const vec& operator/=(double rhs) { x /= rhs; y /= rhs; z /= rhs; return *this; }

  const vec cross(const vec& rhs) const {
    return vec({
      y * rhs.z - z * rhs.y,
      z * rhs.x - x * rhs.z,
      x * rhs.y - y * rhs.x
    });
  }

  double operator*(const vec& rhs) const { return x * rhs.x + y * rhs.y + z * rhs.z; }

#define OPER_DEFINE(oper, opere, type) vec operator oper (type rhs) const { vec result = *this; return (result opere rhs); }
  OPER_DEFINE(+, +=, const vec&);
  OPER_DEFINE(-, -=, const vec&);
  OPER_DEFINE(*, *=, double);
  OPER_DEFINE(/, /=, double);
};
ostream& operator<<(ostream& os, const vec& v) {
  os << "(" << v.x << "," << v.y << "," << v.z << ")";
  return os;
}

vec ang2vec(double zen, double azi) {
    return { sin(zen) * cos(azi), sin(zen) * sin(azi), cos(zen) };
}

bool OnOffWin::count_dAngles2d(double zenc, double azic, double mjd, std::vector<int>& win_ids, std::vector<double>& dAnglesx, std::vector<double>& dAnglesy) {
  zenc *= deg2rad;
  azic *= deg2rad;

  int nlow = (mjd - int(mjd)) / dmjd;
  double mjd_low = int(mjd) + nlow * dmjd,
         mjd_up = int(mjd) + (nlow + 1) * dmjd;

  auto plow = get_point(mjd_low),
       pup = get_point(mjd_up);

  double low_weight = (mjd_up - mjd) / (mjd_up - mjd_low),
         up_weight = 1 - low_weight;
  win_ids.resize(0);
  dAnglesx.resize(0);
  dAnglesy.resize(0);

  int iON = id2i(0);
  double zenc_center = low_weight * plow->second[iON].zenc + up_weight * pup->second[iON].zenc,
         azic_center = low_weight * plow->second[iON].azic + up_weight * pup->second[iON].azic;
  vec center = ang2vec(zenc_center, azic_center);
  vec vlow = ang2vec(plow->second[iON].zenc, plow->second[iON].azic);
  vec vup = ang2vec(pup->second[iON].zenc, pup->second[iON].azic);

  vec vx = vlow - vup; vx /= sqrt(vx * vx);
  vec vy = center.cross(vx);

  for (int i = 0; i < plow->second.size(); i++) {
    int id = i2id(i);
    double zenc_mod = zenc,
           azic_mod = azic - id * dazi(size, zenc_center, rtype);
    double dangle = angle_distance(pi / 2 - zenc_mod, pi / 2 - zenc_center, azic_mod - azic_center);
    if (dangle > size) continue;

    vec v = ang2vec(zenc_mod, azic_mod);
    vec diff = v - center;

    double dx = vx * diff;
    double dy = vy * diff;

    double normfactor = sqrt(diff * diff) / sqrt(dx * dx + dy * dy);
    dx *= normfactor;
    dy *= normfactor;

    win_ids.push_back(i2id(i));
    dAnglesx.push_back(dx * rad2deg);
    dAnglesy.push_back(dy * rad2deg);
  }

  return !win_ids.empty();
}

bool OnOffWin::count_axis(double zenc, double azic, double mjd, std::vector<int>& win_ids, std::vector<double>& ras, std::vector<double>& decs) {
  zenc *= deg2rad;
  azic *= deg2rad;

  win_ids.resize(0);
  ras.resize(0);
  decs.resize(0);

  int nlow = (mjd - int(mjd)) / dmjd;
  double mjd_low = int(mjd) + nlow * dmjd,
         mjd_up = int(mjd) + (nlow + 1) * dmjd;

  auto plow = get_point(mjd_low),
       pup = get_point(mjd_up);

  double low_weight = (mjd_up - mjd) / (mjd_up - mjd_low),
         up_weight = 1 - low_weight;

  double zenc_center, azic_center;
  for (int i = 0; i < plow->second.size(); i++) {
    zenc_center = low_weight * plow->second[i].zenc + up_weight * pup->second[i].zenc;
    azic_center = low_weight * plow->second[i].azic + up_weight * pup->second[i].azic;
    double dangle = angle_distance(pi / 2 - zenc, pi / 2 - zenc_center, azic - azic_center);

    if (dangle > size) continue;
    win_ids.push_back(i2id(i));
  }

  if (win_ids.empty()) return false;

  for (const auto& id : win_ids) {
    coord.set_horizon(zenc, azic - id * dazi(size, zenc_center, rtype), mjd);
    ras.push_back(coord.ra() * rad2deg);
    decs.push_back(coord.dec() * rad2deg);
  }

  return true;
}

void OnOffWin::set_arrange_type(const arrange_type& t) {
  rtype = t;
  traces.clear();
}

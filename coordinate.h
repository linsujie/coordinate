#ifndef COORDINATE_H
#define COORDINATE_H

#include <cmath>

#include "constant.h"

/*******************************************************************
 * This class is used to transform the coordinate information between
 * equator coordinate and the horizontal coordinate
 * All the input and output values are in unit [arc].
 *
 * The normal azimuth convention used inside the class is
 * (N=0,E=90,S=180,W=270 or said NESW).
 * A default transformation between the user's and and the normal
 * convention is provided.
 * The user could require this transformation by specifying the
 * direction (NESW or NWSE) and their origin direction of azimuth
 * (in the normal azimuth convention).
*******************************************************************/
class coordinate {
  public:
    enum azimuth_direction { NESW, NWSE };
    enum transform_method { SOFA, ANA };
  private:
    double ra_, dec_, // [arc]
           lera_, // local earth rotation angle [arc]
           mjd_,
           ra_observer_, // [arc]
           dec_observer_, // [arc]
           alti_observer_, // [m]
           azimuth_origin_,
           zen_,
           azimuth_;
    azimuth_direction direct_;
    transform_method method_;
    static const double credit_mjd;

    void set_equator();
    void set_horizon();
    double assume_mjd(double lera_in) const;

    double azi_user2normal(double azi_user) const { return direct_ == NESW ? azimuth_origin_ + azi_user : azimuth_origin_ - azi_user; }
    double azi_normal2user(double azi_normal) const { return direct_ == NESW ? azi_normal - azimuth_origin_ : azimuth_origin_ - azi_normal; }

    static double angfix(double x) { return x - floor(x / 2 / pi) * 2 * pi;  } // normalize angle to the range [0, 2pi)

  public:
    coordinate(double ra_observer_in = LHAASO_Lngi * deg2rad,
        double dec_observer_in = LHAASO_Lati * deg2rad,
        double alti_observer_in = LHAASO_Alti,
        double azimuth_origin_in = pi / 2 - WCDA_phi * deg2rad,
        azimuth_direction direct_in = NWSE,
        transform_method method = ANA);

    void set_equator(double ra_in, double dec_in, double mjd_in = -1000000);
    void set_horizon(double zen_in, double azimuth_in, double mjd_in = -1000000);

    // The precise calculation would need mjd instead of ERA in fact.
    // However, this rotine would still perform a transformation with a given EAR by assuming mjd ~ credit_mjd 
    // This could be used when we require less precision.
    bool set_lera(double lera_in);
    bool set_mjd(double mjd_in);

    double ra() const { return ra_; }
    double dec() const { return dec_; }
    double lra() const { return lera_; } // The local earth rotation angle in unit [arc].
    double mjd() const { return mjd_; }
    double zen() const { return zen_; }
    double azimuth() const { return angfix(azi_normal2user(azimuth_)); }

    double obslati() const { return dec_observer_; }
    double obslong() const { return ra_observer_; }
    double obsalti() const { return alti_observer_; }
    double obsazimuth() const { return azimuth_origin_; }

  // From Astro.h
  private:
    double gmst_ANA(double mjd);
    void EquToJ2000(double mjd, double ras, double dec, double *ras2000, double *dec2000);
    void J2000ToEqu(double mjd, double ras2000, double dec2000, double *ras, double *dec);
    void equator_horizon(double  mjd, double ras, double dec,double *zen,double *azi);
    void horizon_equator(double mjd, double zen,double azi, double *ras,double *dec);
};

extern double angle_distance(double dec1, double dec2, double delta_ra);

#endif /* COORDINATE_H */

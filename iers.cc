#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include <vector>
#include <set>

#include <dirent.h>

#include "iers.h"

using namespace std;

set<string> ls_dir(const string& dirname) {
	set<string> result;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir (dirname.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL) {
			if (strcmp(".", ent->d_name) && strcmp("..", ent->d_name))
				result.emplace(ent->d_name);
		}
		closedir (dir);
	} else {
		/* could not open directory */
		printf("No file in direction %s is opened.", dirname.c_str());
		return result;
	}

	return result;
}

IERS::IERS(const std::string& bulletinb_path) : start_mjd(0), stat(close) {
  for(auto f : ls_dir(bulletinb_path))
    read_file(bulletinb_path + "/" + f);
}

void IERS::read_file(const std::string& fname) {
  ifstream data(fname);
  string line;
  while(getline(data, line)) read_line(line);
}

bool blank_line(const std::string& line) {
  for (const auto c : line)
    if (c != ' ') return false;
  return true;
}

bool IERS::change_stat(const std::string& line) {
  if (line == " Final values ") stat = final_value;
  else if (line == " Preliminary extension ") stat = prelim_value;
  else if (prelim_value == stat && blank_line(line)) stat = close;
  else return false;

  return true;
}

void IERS::read_line(const std::string& line) {
  if (change_stat(line)) return;
  if (close == stat) return;

  double year, month, day, mjd, _xp, _yp, _dut;
  istringstream is(line);
  is >> year;
  if (blank_line(line) || year == 0) return;

  is >> month >> day >> mjd >> _xp >> _yp >> _dut;
  if (start_mjd == 0) start_mjd = mjd;
  int i = mjd - start_mjd;

  auto push = [&](vector<double>& vec, int ind, double val) {
    vec.resize(ind + 1);
    vec[ind] = val;
  };

  push(final_value == stat ? dut_ : pre_dut_, i, _dut);
  push(final_value == stat ? xp_ : pre_xp_, i, _xp);
  push(final_value == stat ? yp_ : pre_yp_, i, _yp);
}

double IERS::get_value(const std::vector<double>& vec, const std::vector<double>& pre_vec, int mjd) const {
  int i = mjd - start_mjd;

  if (i < vec.size()) return vec[i];
  else if (i < pre_vec.size()) {
    cerr << "WARNING::IERS::operator()::The mjd " << mjd << " is out of the stable IERS result, adopting Preliminary one." << endl;
    return pre_vec[i];
  }

  cerr << "WARNING::IERS::operator()::The mjd " << mjd << " is out of the IERS result, using the last value of the table." << endl;
  return pre_vec[pre_vec.size() - 1];
}

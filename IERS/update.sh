#!/bin/env bash

last_num=`ls bulletinb | sed -e "s:bulletinb-::g" -e "s:.txt::g" | sort -n | tail -n 1`

num=`expr $last_num + 1`
cd bulletinb
while wget https://datacenter.iers.org/data/207/bulletinb-${num}.txt 2> /dev/null
do
  echo ">>The file bulletinb-${num}.txt downloaded."
  num=`expr $num + 1`
done

echo --Updated over. 

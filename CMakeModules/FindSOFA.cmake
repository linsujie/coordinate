# Find SOFA Library (http://http://www.iausofa.org/)
#
# Usage:
# find_package(SOFA [REQUIRED] [QUIET] )
#
# Search path variable:
# SOFA_ROOT_DIR
#
# Once run this will define:
#
# SOFA_FOUND          = system has SOFA lib
# SOFA_LIBRARIES      = full path to the libraries
# SOFA_INCLUDE_DIR    = full path to the header files
#

if(SOFA_ROOT_DIR)
  message("SOFA_ROOT_DIR specified ${SOFA_ROOT_DIR}")
  find_library(SOFA_LIBRARY
    NAMES sofa_c
    PATHS ${SOFA_ROOT_DIR}
    PATH_SUFFIXES lib lib64
    NO_DEFAULT_PATH
    DOC "SOFA library")
  find_path(SOFA_INCLUDE_DIR
    NAMES sofa.h
    PATHS ${SOFA_ROOT_DIR}
    PATH_SUFFIXES  include include/sofa
    NO_DEFAULT_PATH
    DOC "SOFA headers")
else(SOFA_ROOT_DIR)
  find_library(SOFA_LIBRARY
    NAMES sofa_c
    HINTS /usr /usr/local
    PATHS /usr /usr/local
    PATH_SUFFIXES lib lib64
    DOC "SOFA library")
  find_path(SOFA_INCLUDE_DIR
    NAMES sofa.h
    HINTS /usr /usr/local
    PATHS /usr /usr/local
    PATH_SUFFIXES  include include/cfitsio
    DOC "SOFA headers")
endif(SOFA_ROOT_DIR)

set(SOFA_LIBRARIES ${SOFA_LIBRARY})
set(SOFA_INCLUDE_DIRS ${SOFA_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(SOFA DEFAULT_MSG SOFA_LIBRARIES SOFA_INCLUDE_DIRS)

if(SOFA_FOUND)

  # Find the version of the cfitsio header
  FILE(READ "${SOFA_INCLUDE_DIR}/sofa.h" FITSIO_H)
  STRING(REGEX REPLACE ".*SOFA release ([0-9-]+).*" "\\1" SOFA_RELEASE_DATE "${FITSIO_H}")
  message(STATUS "Found SOFA realease date ${SOFA_RELEASE_DATE}")
endif(SOFA_FOUND)

mark_as_advanced(SOFA_LIBRARIES SOFA_INCLUDE_DIRS)

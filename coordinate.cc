#include <cmath>
#include <iostream>

#include <cassert>

#ifndef NO_SOFA
#include <sofa.h>
#include <sofam.h>
#endif

#include "iers.h"
#include "coordinate.h"
#include "conversions.h"

using namespace std;

const double coordinate::credit_mjd = 58850.3;

static IERS iers(COORDINATE_ROOT_DIR "/IERS/bulletinb");

coordinate::coordinate(double ra_observer_in, double dec_observer_in,
    double alti_observer_in,
    double azimuth_origin_in, azimuth_direction direct_in, transform_method method) :
  ra_observer_(ra_observer_in), dec_observer_(dec_observer_in),
  alti_observer_(alti_observer_in),
  azimuth_origin_(azimuth_origin_in), direct_(direct_in), method_(method)
{}

inline bool equal(double x1, double x2) {
  return fabs(x1 - x2) / (fabs(x1) + fabs(x2)) < 1e-13;
}
inline bool set_value(double& x1, const double& x2) {
  if (equal(x1, x2)) return false;
  x1 = x2;
  return true;
}

/*********************************************************
 * For two points with coordinate (Dec1, Ra1) (Dec2, Ra2),
 * the delta angle between them follow the exprssion:
 *       cos(delta) = sin(Dec1)*sin(Dec2) + cos(Dec1)*cos(Dec2)*cos(phi1 - phi2).
*********************************************************/
double angle_distance(double dec1, double dec2, double delta_ra) {
  return acos(sin(dec1) * sin(dec2) + cos(dec1) * cos(dec2) * cos(delta_ra));
}

void coordinate::set_equator() {
  if (method_ == SOFA) {
    #ifdef NO_SOFA
      assert(false && "Error::SOFA is not configured but required here, please check.");
    #else
      double hour, dob, rob, eo;
      iauAtco13(ra_, dec_,
          0, 0, 0, 0,
          2400000.5, mjd_, iers.dUt(mjd_),
          ra_observer_, dec_observer_, alti_observer_,
          iers.xp(mjd_) * DAS2R, iers.yp(mjd_) * DAS2R,
          0, 0, 0, 0.1,
          &azimuth_, &zen_, &hour, &dob, &rob, &eo);
    #endif
  }
  if (method_ == ANA) {
    double ra_earth, dec_earth;
    J2000ToEqu(mjd_, ra_, dec_, &ra_earth, &dec_earth);
    equator_horizon(mjd_, ra_earth, dec_earth, &zen_, &azimuth_);
  }
}
void coordinate::set_equator(double ra_in, double dec_in, double mjd_in) {
  bool ra_set = set_value(ra_, ra_in),
       dec_set = set_value(dec_, dec_in),
       mjd_set = set_mjd(mjd_in);
  if (!(ra_set || dec_set || mjd_set)) return;

  set_equator();
}

bool coordinate::set_mjd(double mjd_in) {
  if (mjd_in <= -1000000) return true;
  if (!set_value(mjd_, mjd_in)) return false;
  lera_ = lera(mjd_);
  return true;
}

double coordinate::assume_mjd(double lera_in) const {
  const double dmjd = 0.2,
        era_low = lera(credit_mjd),
        era_up = lera(credit_mjd + dmjd) > era_low ? lera(credit_mjd + dmjd) : lera(credit_mjd + dmjd) + 2 * pi;
  return credit_mjd + (lera_in - era_low) * dmjd / (era_up - era_low);
}

bool coordinate::set_lera(double lera_in) {
  if (!set_value(lera_, lera_in)) return false;
  mjd_ = assume_mjd(lera_);
  return true;
}

void coordinate::set_horizon() {
  if (method_ == SOFA)
    #ifdef NO_SOFA
      assert(false && "Error::SOFA is not configured but required here, please check.");
    #else
      iauAtoc13("A", azimuth_, zen_, 2400000.5, mjd_, iers.dUt(mjd_),
          ra_observer_, dec_observer_, alti_observer_,
          iers.xp(mjd_) * DAS2R, iers.yp(mjd_) * DAS2R,
          0, 0, 0, 0.1,
          &ra_, &dec_);
    #endif
  if (method_ == ANA) {
    horizon_equator(mjd_, zen_, azimuth_, &ra_, &dec_);
    EquToJ2000(mjd_, ra_, dec_, &ra_, &dec_);
  }
}

void coordinate::set_horizon(double zen_in, double azimuth_in, double mjd_in) {
  bool zen_set = set_value(zen_, zen_in),
       azimuth_set = set_value(azimuth_, azi_user2normal(azimuth_in)),
       mjd_set = set_mjd(mjd_in);
  if (!(zen_set || azimuth_set || mjd_set)) return;

  set_horizon();
}

// Astro.h start

double coordinate::gmst_ANA(double mjd) {
  double ut1, Tu, alpham;

  ut1 = mjd - floor(mjd);
  Tu = (mjd - 51544.5) / 36525.0;
  alpham = 280.460618370 
    + 36000.770053608 * Tu 
    + 3.8793333331e-4 * Tu * Tu 
    - 2.5833333331e-8 * Tu * Tu * Tu;

  return (180.0 + ut1 * 360.0  + alpham);
}

void coordinate::EquToJ2000(double mjd, double ras, double dec, double *ras2000, double *dec2000) {
  /*
  mjd     : input  : Modified Julian Day
  ra      : input  : right ascension at the day [deg]
  dec     : input  : declination at the day [deg]
  ra2000  : output : right ascension at J2000 [deg]
  dec2000 : output : declination at J2000 [deg]
  */
  
  double x, y, z, x2000, y2000, z2000;
  double p11, p12, p13, p21, p22, p23, p31, p32, p33;
  double zeta, zee, theta, tu, tu2, tu3;
  double sinzeta, coszeta, sinzee, coszee, sintheta, costheta;

  tu = (mjd - 51544.5) / 36525.0;
  tu2 = tu * tu;
  tu3 = tu2 * tu;
  zeta  = 0.0111808609 * tu + 1.463556e-6 * tu2 + 8.725677e-8 * tu3;
  zee   = 0.0111808609 * tu + 5.307158e-6 * tu2 + 8.825063e-8 * tu3;
  theta = 0.0097171735 * tu - 2.068458e-6 * tu2 - 2.028121e-7 * tu3;

  sinzeta  = sin(zeta);  coszeta  = cos(zeta);
  sinzee   = sin(zee);   coszee   = cos(zee);
  sintheta = sin(theta); costheta = cos(theta);

  p11 =  coszeta * costheta * coszee - sinzeta * sinzee;
  p12 = -sinzeta * costheta * coszee - coszeta * sinzee;
  p13 = -sintheta * coszee;
  p21 =  coszeta * costheta * sinzee + sinzeta * coszee;
  p22 = -sinzeta * costheta * sinzee + coszeta * coszee;
  p23 = -sintheta * sinzee;
  p31 =  coszeta * sintheta;
  p32 = -sinzeta * sintheta;
  p33 =  costheta;

  x = cos(dec) * cos(ras);
  y = cos(dec) * sin(ras);
  z = sin(dec);
  x2000 = p11 * x + p21 * y + p31 * z;
  y2000 = p12 * x + p22 * y + p32 * z;
  z2000 = p13 * x + p23 * y + p33 * z;

  *ras2000 = atan2(y2000, x2000);
  if (*ras2000 < 0.0) *ras2000 += 2 * pi;
  *dec2000 = asin(z2000);
}

void coordinate::J2000ToEqu(double mjd, double ras2000, double dec2000, double *ras, double *dec) {
  double x, y, z, x2000, y2000, z2000;
  double p11, p12, p13, p21, p22, p23, p31, p32, p33;
  double zeta, zee, theta, tu, tu2, tu3;
  double sinzeta, coszeta, sinzee, coszee, sintheta, costheta;

  tu = (mjd - 51544.5) / 36525.0;
  tu2 = tu * tu;
  tu3 = tu2 * tu;
  zeta  = 0.0111808609 * tu + 1.463556e-6 * tu2 + 8.725677e-8 * tu3;
  zee   = 0.0111808609 * tu + 5.307158e-6 * tu2 + 8.825063e-8 * tu3;
  theta = 0.0097171735 * tu - 2.068458e-6 * tu2 - 2.028121e-7 * tu3;

  sinzeta  = sin(zeta);  coszeta  = cos(zeta);
  sinzee   = sin(zee);   coszee   = cos(zee);
  sintheta = sin(theta); costheta = cos(theta);

  p11 =  coszeta * costheta * coszee - sinzeta * sinzee;
  p12 = -sinzeta * costheta * coszee - coszeta * sinzee;
  p13 = -sintheta * coszee;
  p21 =  coszeta * costheta * sinzee + sinzeta * coszee;
  p22 = -sinzeta * costheta * sinzee + coszeta * coszee;
  p23 = -sintheta * sinzee;
  p31 =  coszeta * sintheta;
  p32 = -sinzeta * sintheta;
  p33 =  costheta;

  x2000 = cos(dec2000) * cos(ras2000);
  y2000 = cos(dec2000) * sin(ras2000);
  z2000 = sin(dec2000);
  x = p11 * x2000 + p12 * y2000 + p13 * z2000;
  y = p21 * x2000 + p22 * y2000 + p23 * z2000;
  z = p31 * x2000 + p32 * y2000 + p33 * z2000;

  *ras = atan2(y, x);
  if (*ras < 0.0) *ras += 2 * pi;
  *dec = asin(z);
}

void coordinate::equator_horizon(double mjd, double ras, double dec, double *zen,double *azi) {
  double lst, H, g;
  double sH, sdec, sphi;
  double cH, cdec, cphi;
  double zenith, azimuth;
  double sazi,cazi;

  g = gmst_ANA(mjd) * deg2rad + ra_observer_;
  lst = (g - floor(g / (2 * pi)) * (2 * pi));
  H = lst - ras;

  sH = sin(H);
  sdec = sin(dec);
  sphi = sin(dec_observer_);
  cH = cos(H);
  cdec = cos(dec);
  cphi = cos(dec_observer_);

  zenith = acos(sphi *  sdec + cphi * cdec * cH);

  sazi = (-cdec * sH) / sin(zenith);
  cazi = (cphi * sdec - sphi * cdec * cH) / sin(zenith);

  azimuth = atan2(sazi, cazi);
  if(azimuth > pi) azimuth -= 2 * pi;
  if(azimuth < -pi) azimuth += 2 * pi;

  *zen=zenith;
  *azi=azimuth;
}

void coordinate::horizon_equator(double mjd, double zen,double azi, double *ras,double *dec) {
  double lst, H, g;
  double sazi, szen, sphi;
  double cazi, czen, cphi;
  double DEC, RAS;
  double sH, cH;

  g = gmst_ANA(mjd) * deg2rad + ra_observer_;
  lst = (g - floor(g / (2 * pi)) * (2 * pi));

  sazi = sin(azi);
  szen = sin(zen);
  sphi = sin(dec_observer_);
  cazi = cos(azi);
  czen = cos(zen);
  cphi = cos(dec_observer_);

  DEC = asin(sphi * czen + cphi * szen * cazi);

  sH = (-szen * sazi) / cos(DEC);
  cH = (cphi * czen - sphi * szen * cazi) / cos(DEC);

  H = atan2(sH, cH);

  RAS = lst - H;
  if(RAS > 2 * pi) RAS -= 2 * pi;
  if(RAS < 0.0) RAS += 2 * pi;

  *ras=RAS;
  *dec=DEC;
}

// Astro.h end

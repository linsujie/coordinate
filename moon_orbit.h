#ifndef MOON_ORBIT_H
#define MOON_ORBIT_H

//                               rad           rad
void moon_orbit(double mjd, double& MRAS, double& MDEC);

#endif /* MOON_ORBIT_H */

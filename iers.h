#ifndef IERS_H
#define IERS_H

#include <cstring>
#include <vector>

/*****************************************************************
 * The class to read the IERS bulletinb information and return
 * the corresponding pole x [mas], pole y [mas], and dUt[s] for specified MJD
 * ***************************************************************/
class IERS {
  enum read_stat { final_value, prelim_value, close };
  std::vector<double> xp_, pre_xp_;
  std::vector<double> yp_, pre_yp_;
  std::vector<double> dut_, pre_dut_;
  int start_mjd;
  read_stat stat;

  bool change_stat(const std::string& line);
  void read_file(const std::string& fname);
  void read_line(const std::string& line);

  double get_value(const std::vector<double>& vec, const std::vector<double>& pre_vec, int mjd) const;

  public:
  IERS(const std::string& bulletinb_path);
  double xp(int mjd) const { return get_value(xp_, pre_xp_, mjd) / 1000; } // [as]
  double yp(int mjd) const { return get_value(yp_, pre_yp_, mjd) / 1000; } // [as]
  double dUt(int mjd) const { return get_value(dut_, pre_dut_, mjd) / 1000; } // [s]
};

#endif /* IERS_H */
